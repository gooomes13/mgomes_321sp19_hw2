<?php
    // get the data from the form
    $product_description = $_POST['product_description'];
    $list_price = $_POST['list_price'];
    $quantity = $_POST['quantity'];
    $sales_tax = $_POST['sales_tax'];
    
    
    // calculate the total cost of all items
    $total_price = (list_price * $quantity)+(list_price*quantity*sales_tax);
    
    // apply currency formatting to the dollar and total amounts
    $list_price_formatted = "$".number_format($list_price, 2);
    $quantity_formatted = quantity;
    $total_price_formatted = "$".number_format($total_price, 2);   
    $sales_tax_formatted = sales_tax;
    
    // escape the unformatted input
    $product_description_escaped = htmlspecialchars($product_description);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Total Cost After Tax Calculator</title>
    <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\book_apps\mgomes_321sp19_hw2\styles\main.css">
</head>
<body>
    <main>
        <h1>Total Cost After Tax Calculator</h1>

        <label>Product Description:</label>
        <span><?php echo $product_description_escaped; ?></span><br>

        <label>List Price:</label>
        <span><?php echo $list_price_formatted; ?></span><br>

        <label>Number of items:</label>
        <span><?php echo $quantity_formatted; ?></span><br>
        
        <label>Sales Tax:</label>
        <span><?php echo $sales_tax_formatted; ?></span><br>

        <label>Final Price:</label>
        <span><?php echo $discount_price_formatted; ?></span><br>
    </main>
</body>
</html>