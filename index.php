<!DOCTYPE html>
<html>
<head>
    <title>Total Cost After Tax Calculator</title>
    <!-- always make sure file paths are coded correctly -->
    <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\book_apps\mgomes_321sp19_hw2\styles.main.css">
</head>

<body>
    <main> <!--Calculates total cost when multiple of the same item purchased. -->
        <h1>Total Cost Calculator</h1>
        <form action="results.php" method="post">

            <div id="data">
                <label>Product Description:</label>
                <input type="text" name="product_description"><br>

                <label>List Price:</label>
                <input type="text" name="list_price"><br>
                <!-- added tax functionality-->
                <label>Sales Tax:</label>
                <input type="text" name="Sales Tax"><span>%</span><br>

                <label>Quantity:</label>
                <input type="text" name="quantity"><span>%</span><br>
            </div>

            <div id="buttons">
                <label>&nbsp;</label>
                <input type="submit" value="Calculate Total Value"><br>
            </div>

        </form>
    </main>
</body>
</html>